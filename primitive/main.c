#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

extern int print_sizes(void);
extern int print_endianness(void);
extern int test_sscanf(void);
extern int to_tlp224(unsigned char* cmd, const unsigned char len, char* const dest);
extern int from_tlp224(const char* const ascii, int length, unsigned char* const dest);

#define print_sizeof(TYPE) printf("sizeof(" #TYPE ") = %lu\n", sizeof(TYPE))

int print_sizes(void) 
{
	print_sizeof(char);
	print_sizeof(short);
	print_sizeof(int);
	print_sizeof(long);
	print_sizeof(long long);
	print_sizeof(void*);
	print_sizeof(float);
	print_sizeof(double);
	print_sizeof(long double);
	return 0;
}

int print_endianness(void) 
{
	unsigned int subject = 0xFFAA;
	unsigned char* pt = (unsigned char*) &subject;
	if (*pt == 0xFF)
		printf("Big Endian Arch!\n");
	else if (*pt == 0xAA) 
		printf("Small Endian Arch!\n");
	else
		printf("Failure?!\n");
	return 0;
}

int test_sscanf(void)
{
	char input[] = "FFEEDDCCBBAA99";
	unsigned char buffer[8];
	unsigned long int* trsfmr = (unsigned long int*) buffer;

	memset((void*)trsfmr, 0, 8);

	sscanf(input, "%16lx", (long unsigned int*) buffer);

	printf("The result is: 0x%lx\n", *trsfmr);
	return 0;
}

int from_tlp224(const char* const ascii, int length, unsigned char* const dest)
{
	unsigned char ack, lxc, len, i, expected;
	sscanf(ascii, "%2hhx%2hhx", &ack, &len);

	assert(len == (length - 1) / 2 - 3);

	for(i = 0; i < len; i++)
		sscanf(ascii + 2 * (2 + i), "%2hhx", &dest[i]);

	sscanf(ascii + 2 * (2 + len), "%2hhx", &expected);

	// verification the checksum
	lxc = ack ^ len;
	for (i = 0; i < len; i++)
		lxc ^= dest[i];
	assert(lxc == expected);

	return len;
}

int to_tlp224(unsigned char* cmd, const unsigned char len, char* const dest)
{
	static unsigned char ack = 0x60;
	unsigned char lxc = 0;
	int i = 0;

	// calculate the checksum
	lxc = ack ^ len;
	for(i = 0; i < len; i++)
		lxc ^= cmd[i];

	// append the header
	sprintf(dest, "%02X%02X", ack, len);

	// append the body
	for(i = 0; i < len; i++)
		sprintf(dest + 2 * (i + 2), "%02X", cmd[i]);

	// append the checksum
	sprintf(dest + 2 * (len + 2), "%02X", lxc); 

	// append the EOT
	dest[2 * (len + 3)] = 0x03;
	return 2 * (len + 3) + 1;
}

int main(int argc, char* argv[])
{
	unsigned char cmd[] = {0xE2, 0x10};
	char buffer[256];
	unsigned char resp[16];
	int ret, i;

	memset((void*)buffer, 0, sizeof(buffer));
	memset((void*)resp, 0, sizeof(resp));

	print_sizes();
	print_endianness();
	test_sscanf();

	ret = to_tlp224(cmd, sizeof(cmd), buffer);

	printf("=============>The result is [%s]\n", buffer);

	ret = from_tlp224(buffer, ret, resp);
	printf("<============The response is:[0x");
	for(i = 0; i < ret; i++)
		printf("%02X", resp[i]);
	printf("]\n");

	return 1;
}
