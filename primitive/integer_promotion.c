#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

extern int case_char(void);
extern int case_int(void);
extern int case_uint(void);
extern int case_uint2(void);
int case_char(void)
{
	char cresult, c1, c2, c3;
	c1 = SCHAR_MAX;
	c2 = 3;
	c3 = 3;
	cresult = c1 * c2 / c3;
	printf("====>c1=%d,result=%d\n", c1, cresult);
	return 0;
}

int case_uint2(void)
{
	int cresult;
	int c1 = INT_MAX;
	cresult = c1 * 2 / 2;
	printf("====>c1=0x%x, result=0x%x\n", c1, cresult);
	return 0;
}

int case_int(void)
{
	int cresult;
	int c1 = INT_MAX;
	int c2 = INT_MAX;
	cresult = c1 + c2;
	if (cresult < c1)
		printf("overflow!\n");
	printf("====>c1=0x%x, result=0x%x\n", c1, cresult);
	return 0;
}

int case_uint(void)
{
	unsigned int cresult;
	unsigned int c1 = UINT_MAX;
	unsigned int c2 = UINT_MAX;
	cresult = c1 + c2;
	if (cresult < c1)
		printf("overflow!\n");
	printf("====>c1=0x%x, result=0x%x\n", c1, cresult);
	return 0;
}

int main(int argc, char* argv)
{
	// case_char();
	// case_uint();
	// case_int();
	case_uint2();
}

