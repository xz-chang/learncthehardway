#include <stdio.h>
#include <stdlib.h>
#include <glib.h>
#include <string.h>
#include "array.h"
#include "iso7816.h"

void array_main(void)
{
	guint i;
	unsigned char cmd_select[] = {0x00, 0xA4, 0x00, 0x00, 0x00};
	unsigned char cmd_select2[] = {0x80, 0xA4, 0x00, 0x00, 0x00};
	ISO7816 select, select2;
	__iso7816_self_test();
	iso7816_init(&select);
	memcpy(&select, cmd_select, sizeof(cmd_select)); 
	iso7816_init(&select2);
	memcpy(&select2, cmd_select2, sizeof(cmd_select2)); 
	GArray* cmds = g_array_new(FALSE, TRUE, sizeof(ISO7816));
	g_array_insert_val(cmds, 0, select);
	g_array_insert_val(cmds, 1, select2);

	printf("====>This size is %u\n", g_array_get_element_size(cmds));
	printf("%s\n", iso7816_print(&g_array_index(cmds, ISO7816, 0)));
	printf("%s\n", iso7816_print(&g_array_index(cmds, ISO7816, 1)));

	for(i = 0; i < 2; i++)
		printf("%s\n", iso7816_print(&g_array_index(cmds, ISO7816, i)));

	g_array_free(cmds, FALSE);

	iso7816_destroy(&select2);
	iso7816_destroy(&select);
}
