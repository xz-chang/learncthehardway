#include <stdio.h>
#include <stdlib.h>
#include <glib.h>
#include "list.h"
#include "array.h"

int main(int argc, char* argv[])
{
	list_main();
	array_main();
	return 1;
}
