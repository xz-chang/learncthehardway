#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <string.h>
#include "iso7816.h"

int __iso7816_self_test(void) {
	printf("offsetof(ISO7816, cla)=%lu\n", offsetof(ISO7816, cla));
	printf("offsetof(ISO7816, ins)=%lu\n", offsetof(ISO7816, ins));
	printf("offsetof(ISO7816, p1)=%lu\n", offsetof(ISO7816, p1));
	printf("offsetof(ISO7816, p2)=%lu\n", offsetof(ISO7816, p2));
	printf("offsetof(ISO7816, lc)=%lu\n", offsetof(ISO7816, lc));
	printf("offsetof(ISO7816, data)=%lu\n", offsetof(ISO7816, data));
	printf("offsetof(ISO7816, le)=%lu\n", offsetof(ISO7816, le));
	return 1;
}

int iso7816_init(ISO7816* this) {
	memset(this, 0, sizeof(ISO7816));
	this->data = calloc(ISO7816_MAX_DATA_LEN, sizeof(unsigned char));
	return 1;
}

int iso7816_destroy(ISO7816* this) {
	free(this->data);
	return 1;
}

char* iso7816_print(ISO7816* this) {
	static char output_buffer[256];// add some redundancy
	memset(output_buffer, 0, 256);
	sprintf(output_buffer, "0x%02X%02X%02X%02X%02X%02X", 
			this->cla,
			this->ins,
			this->p1,
			this->p2,
			this->lc,
			this->le);
	return output_buffer;
}
