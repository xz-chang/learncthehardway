#ifndef __iso7816_h__
#define __iso7816_h__

#define ISO7816_SW1SW2_SUCCESS 0X9000
#define ISO7816_MAX_DATA_LEN 1024

/*
 * Defined in ISO/IEE-7816-4 protocol suit
 * TODO: the lc & le items can be 3 bytes, 
 * which is not handled now.
 *
 * Use __packed__ to make sure no compiler optimization
 */
typedef struct __attribute__((__packed__)){
	unsigned char cla;
	unsigned char ins;
	unsigned char p1;
	unsigned char p2;
	// can be 0, 1, 3 bytes
	unsigned char lc; 
	unsigned char* data;
	// can be 0, 1, 3, bytes
	unsigned char le; 
} ISO7816;

extern int __iso7816_self_test(void);
extern int iso7816_init(ISO7816* this); 
extern int iso7816_destroy(ISO7816* this);
extern char* iso7816_print(ISO7816* this); 

#endif

